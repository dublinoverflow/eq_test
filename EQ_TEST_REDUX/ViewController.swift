//
//  ViewController.swift
//  EQ_TEST_REDUX
//
//  Created on 4/30/19.
//  All rights reserved.
//

import UIKit
import AudioKit
import AudioKitUI



class ViewController: UIViewController {
    
    var mainView: UIView!
    var testAudioFile: AKAudioFile!
    var audioPlayer: AKAudioPlayer!
    var mixer: AKMixer!
    
    
    // EQ bands
    var filterBand2: AKEqualizerFilter!
    var filterBand3: AKEqualizerFilter!
    var filterBand4: AKEqualizerFilter!
    var filterBand5: AKEqualizerFilter!
    var filterBand6: AKEqualizerFilter!
    var filterBand7: AKEqualizerFilter!
    var filterBand8: AKEqualizerFilter!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        // programmatically setup UI
        mainView = UIView()
        mainView.backgroundColor = #colorLiteral(red: 0.05319298998, green: 0.5662047318, blue: 0.9686274529, alpha: 1)
        self.view.addSubview(mainView)
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        mainView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        mainView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        
        
        
        do {
            testAudioFile = try AKAudioFile(readFileName: "BurnRightDown.mp3")
            audioPlayer = try AKAudioPlayer(file: testAudioFile)
            
            // SETUP EQs
            filterBand3 = AKEqualizerFilter(audioPlayer, centerFrequency: 64, bandwidth: 70.8, gain: 1.0)
            filterBand4 = AKEqualizerFilter(filterBand3, centerFrequency: 125, bandwidth: 141, gain: 1.0)
            filterBand5 = AKEqualizerFilter(filterBand4, centerFrequency: 250, bandwidth: 282, gain: 1.0)
            filterBand6 = AKEqualizerFilter(filterBand5, centerFrequency: 500, bandwidth: 562, gain: 1.0)
            filterBand7 = AKEqualizerFilter(filterBand6, centerFrequency: 1_000, bandwidth: 1_112, gain: 1.0)
            filterBand8 = AKEqualizerFilter(filterBand7, centerFrequency: 2_000, bandwidth: 1_512, gain: 1.0)
            
            mixer = AKMixer(filterBand8)
            audioPlayer.connect(to: mixer.inputNode)
            AudioKit.output = mixer

        } catch {
            print("problem setting up audiokit: \(error)")

        }
        
        
        // play audio btn
        let button = UIButton()
        mainView.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -40).isActive = true
        button.widthAnchor.constraint(equalToConstant: 70).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        button.layer.cornerRadius = 5
        button.setTitle("Play", for: .normal)
        button.addTarget(self, action: #selector(playClicked), for: .touchUpInside)
        
        // save EQ'd audio btn
        let saveEQAudio = UIButton()
        mainView.addSubview(saveEQAudio)
        saveEQAudio.translatesAutoresizingMaskIntoConstraints = false
        saveEQAudio.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 100).isActive = true
        saveEQAudio.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -40).isActive = true
        saveEQAudio.widthAnchor.constraint(equalToConstant: 70).isActive = true
        saveEQAudio.heightAnchor.constraint(equalToConstant: 50).isActive = true
        saveEQAudio.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        saveEQAudio.layer.cornerRadius = 5
        saveEQAudio.setTitle("save", for: .normal)
        saveEQAudio.addTarget(self, action: #selector(saveClicked), for: .touchUpInside)
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        do {
            try AudioKit.start()
            
        } catch {
            AKLog("AudioKit did not start!")
        }
        
        // setup all EQ sliders and AutoLayout stuff
        setupEQUI(mainView: mainView)
        
    }
    
    
    @objc func playClicked(sender: UIButton){
        
        sender.pulsate()
        
        if audioPlayer.isPlaying {
            audioPlayer.pause()
            sender.setTitle("Play", for: .normal)
            print("AKPlayer is paused.")
        } else {
            audioPlayer.play()
            sender.setTitle("Stop", for: .normal)
            print("AKPlayer is playing............")
            
        }
    
    }
    
    
    @objc func saveClicked(sender: UIButton){
        
        sender.pulsate()
        
        if audioPlayer.isPlaying {
            audioPlayer.pause()
            print("AKPlayer is paused.")
        } else {
            audioPlayer.play()
            print("AKPlayer is playing............")
        }
        
        // trying to renderToFile() w/effect
        renderEQAudio()
        
    }
    
    


}


extension ViewController {
    
    // https://audiokit.io/playgrounds/Effects/Graphic%20Equalizer/
    
    func setupEQUI(mainView: UIView) {
        
        let AKSliderFilter3 = AKSlider(property: "60Hz", value: self.filterBand3.gain, range: 0 ... 4) { sliderValue in
            self.filterBand3.gain = sliderValue
        }
        mainView.addSubview(AKSliderFilter3)
        
        AKSliderFilter3.translatesAutoresizingMaskIntoConstraints = false
        AKSliderFilter3.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        AKSliderFilter3.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -100).isActive = true
        AKSliderFilter3.widthAnchor.constraint(equalToConstant: 250).isActive = true
        AKSliderFilter3.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let AKSliderFilter4 = AKSlider(property: "125Hz", value: self.filterBand4.gain, range: 0 ... 4) { sliderValue in
            self.filterBand4.gain = sliderValue
        }
        mainView.addSubview(AKSliderFilter4)
        
        
        AKSliderFilter4.translatesAutoresizingMaskIntoConstraints = false
        AKSliderFilter4.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        AKSliderFilter4.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -150).isActive = true
        AKSliderFilter4.widthAnchor.constraint(equalToConstant: 250).isActive = true
        AKSliderFilter4.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let AKSliderFilter5 = AKSlider(property: "250Hz", value: self.filterBand5.gain, range: 0 ... 4) { sliderValue in
            self.filterBand5.gain = sliderValue
        }
        mainView.addSubview(AKSliderFilter5)
        
        AKSliderFilter5.translatesAutoresizingMaskIntoConstraints = false
        AKSliderFilter5.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        AKSliderFilter5.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -200).isActive = true
        AKSliderFilter5.widthAnchor.constraint(equalToConstant: 250).isActive = true
        AKSliderFilter5.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let AKSliderFilter6 = AKSlider(property: "500Hz", value: filterBand6.gain, range: 0 ... 4) { sliderValue in
            self.filterBand6.gain = sliderValue
        }
        mainView.addSubview(AKSliderFilter6)
        
        AKSliderFilter6.translatesAutoresizingMaskIntoConstraints = false
        AKSliderFilter6.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        AKSliderFilter6.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -250).isActive = true
        AKSliderFilter6.widthAnchor.constraint(equalToConstant: 250).isActive = true
        AKSliderFilter6.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let AKSliderFilter7 = AKSlider(property: "1KHz", value: filterBand7.gain, range: 0 ... 4) { sliderValue in
            self.filterBand7.gain = sliderValue
        }
        mainView.addSubview(AKSliderFilter7)
        
        AKSliderFilter7.translatesAutoresizingMaskIntoConstraints = false
        AKSliderFilter7.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        AKSliderFilter7.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -300).isActive = true
        AKSliderFilter7.widthAnchor.constraint(equalToConstant: 250).isActive = true
        AKSliderFilter7.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        let AKSliderFilter8 = AKSlider(property: "2KHz", value: filterBand8.gain, range: 0 ... 4) { sliderValue in
            self.filterBand8.gain = sliderValue
        }
        mainView.addSubview(AKSliderFilter8)
        
        AKSliderFilter8.translatesAutoresizingMaskIntoConstraints = false
        AKSliderFilter8.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        AKSliderFilter8.centerYAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -350).isActive = true
        AKSliderFilter8.widthAnchor.constraint(equalToConstant: 250).isActive = true
        AKSliderFilter8.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    
    
    
    
    
    // source: https://stackoverflow.com/questions/52633424/audiokit-export-file-with-filters
    // trying to render file with effects (need to goggle this in @objc func saveClicked)
    func renderEQAudio() {
        
        print("Trying to render EQ'd audio.....")
        
        do {
            // think we need to actually initialize an AVAudioFile for the first argument?
            // looking at source code:
            // https://github.com/AudioKit/AudioKit/blob/79cebe3ff8acc64e743b9e9536693c193e478eb4/AudioKit/Common/Internals/AVAudioEngine%2BExtensions.swift#L24
            // first param is an file initialized for writing
            // also, seem to only be able to call .renderToFile() on AudioKit but not any of the other AudioKit objects here
            let createdAudioFile = createAVAudioFile()
            
            
            let tape = try AKAudioFile(forReading: createdAudioFile.url)
            
            let testPlayer = try AKAudioPlayer(file: tape)
            
            try AudioKit.renderToFile(tape, duration: 10.0, prerender: {testPlayer.play()})
            
            
            //presentPlayerAfterRender(audioFile)
            
            
        } catch {
            print("error occured during renderToFile(): \(error)")
        }
    }
    
    
    
    
    
    
    // we need to initialize an AVAudioFile first for .renderToFile() to write to?
    // see starting at line 261
    // https://github.com/mattneub/Programming-iOS-Book-Examples/blob/master/bk2ch14p655AVAudioEngineTest/AVAudioEngineTest/ViewController.swift
    func createAVAudioFile() -> AVAudioFile {
        
        print("creating AVAudioFile.....")
        
        let fm = FileManager.default
        let doc = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let outurl = doc.appendingPathComponent("myTestFile.mp4", isDirectory:false)
        try? fm.removeItem(at: outurl) // just in case
        let outfile = try! AVAudioFile(forWriting: outurl, settings: [
            AVFormatIDKey : kAudioFormatMPEG4AAC,
            AVNumberOfChannelsKey : 1,
            AVSampleRateKey : 22050,
            ])
        
        return outfile
        
    }
    
    
    
    
    
    
    
}


extension UIButton {
    
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.1
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 2
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: nil)
    }
}

